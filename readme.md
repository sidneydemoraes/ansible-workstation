# How to run
`ansible-playbook <yml-file> --ask-vault-password --ask-become-pass [--tags <tags-separated-by-comma>]`
